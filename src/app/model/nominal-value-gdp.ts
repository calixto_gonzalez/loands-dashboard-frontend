class Series {
  date?: string;
  name: string;
  value: string | number;
}


export class NominalValueGDP {
  name: string;
  description: string;
  refreshedAt: string;
  newestAvailableDate: string;
  oldestAvailableDate: string;
  frequency: string;
  type: string;
  startDate: Date;
  endDate: Date;
  series: Series[];
  data: Series[];
}
