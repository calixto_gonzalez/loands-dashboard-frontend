import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {NominalValueGDP} from '../model/nominal-value-gdp';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  const;
  // mockData = {
  //   name: '089.Loans (PSDCGGB) US$ - Netherlands',
  //   description: '089.Loans (PSDCGGB) US$ - Netherlands. ',
  //   refreshedAt: '2020-05-24T18:47:13.442',
  //   newestAvailableDate: '2019-12-31',
  //   oldestAvailableDate: '1995-12-31',
  //   frequency: 'quarterly',
  //   type: 'Time Series',
  //   startDate: '1995-12-31',
  //   endDate: '2019-12-31',
  //   data: [
  //     {
  //       name: '2019-12-31',
  //       value: 3.42030364E10
  //     },
  //     {
  //       name: '2019-09-30',
  //       value: 3.37003661E10
  //     },
  //     {
  //       name: '2019-06-30',
  //       value: 3.4133172E10
  //     },
  //     {
  //       name: '2019-03-31',
  //       value: 3.31983015E10
  //     },
  //     {
  //       name: '2018-12-31',
  //       value: 3.304012E10
  //     },
  //     {
  //       name: '2018-09-30',
  //       value: 3.51898824E10
  //     },
  //     {
  //       name: '2018-06-30',
  //       value: 3.52223154E10
  //     },
  //     {
  //       name: '2018-03-31',
  //       value: 3.94974297E10
  //     },
  //     {
  //       name: '2017-12-31',
  //       value: 3.9325047E10
  //     },
  //     {
  //       name: '2017-09-30',
  //       value: 4.1037656E10
  //     },
  //     {
  //       name: '2017-06-30',
  //       value: 4.21353864E10
  //     },
  //     {
  //       name: '2017-03-31',
  //       value: 3.90200118E10
  //     },
  //     {
  //       name: '2016-12-31',
  //       value: 4.18804471E10
  //     },
  //     {
  //       name: '2016-09-30',
  //       value: 4.59900166E10
  //     },
  //     {
  //       name: '2016-06-30',
  //       value: 4.70436148E10
  //     },
  //     {
  //       name: '2016-03-31',
  //       value: 4.9383576E10
  //     },
  //     {
  //       name: '2015-12-31',
  //       value: 4.51799613E10
  //     },
  //     {
  //       name: '2015-09-30',
  //       value: 4.85818095E10
  //     },
  //     {
  //       name: '2015-06-30',
  //       value: 4.62284724E10
  //     },
  //     {
  //       name: '2015-03-31',
  //       value: 5.28040961E10
  //     },
  //     {
  //       name: '2014-12-31',
  //       value: 4.89731517E10
  //     },
  //     {
  //       name: '2014-09-30',
  //       value: 4.63155064E10
  //     },
  //     {
  //       name: '2014-06-30',
  //       value: 4.55794776E10
  //     },
  //     {
  //       name: '2014-03-31',
  //       value: 4.66751376E10
  //     },
  //     {
  //       name: '2013-12-31',
  //       value: 5.00516763E10
  //     },
  //     {
  //       name: '2013-09-30',
  //       value: 4.79278945E10
  //     },
  //     {
  //       name: '2013-06-30',
  //       value: 4.942932E10
  //     },
  //     {
  //       name: '2013-03-31',
  //       value: 5.37438655E10
  //     },
  //     {
  //       name: '2012-12-31',
  //       value: 5.75759772E10
  //     },
  //     {
  //       name: '2012-09-30',
  //       value: 5.6645037E10
  //     },
  //     {
  //       name: '2012-06-30',
  //       value: 5.1561086E10
  //     },
  //     {
  //       name: '2012-03-31',
  //       value: 4.72909248E10
  //     },
  //     {
  //       name: '2011-12-31',
  //       value: 4.43406591E10
  //     },
  //     {
  //       name: '2011-09-30',
  //       value: 4.33932408E10
  //     },
  //     {
  //       name: '2011-06-30',
  //       value: 3.77295565E10
  //     },
  //     {
  //       name: '2011-03-31',
  //       value: 4.49196926E10
  //     },
  //     {
  //       name: '2010-12-31',
  //       value: 3.89622558E10
  //     },
  //     {
  //       name: '2010-09-30',
  //       value: 4.44296992E10
  //     },
  //     {
  //       name: '2010-06-30',
  //       value: 4.32908609E10
  //     },
  //     {
  //       name: '2010-03-31',
  //       value: 4.08481095E10
  //     },
  //     {
  //       name: '2009-12-31',
  //       value: 4.79316432E10
  //     },
  //     {
  //       name: '2009-09-30',
  //       value: 5.40853848E10
  //     },
  //     {
  //       name: '2009-06-30',
  //       value: 6.78262392E10
  //     },
  //     {
  //       name: '2009-03-31',
  //       value: 5.7337518E10
  //     },
  //     {
  //       name: '2008-12-31',
  //       value: 2.72856702E10
  //     },
  //     {
  //       name: '2008-09-30',
  //       value: 1.90959352999999E10
  //     },
  //     {
  //       name: '2008-06-30',
  //       value: 1.48670284E10
  //     },
  //     {
  //       name: '2008-03-31',
  //       value: 1.60017439999999E10
  //     },
  //     {
  //       name: '2007-12-31',
  //       value: 1.30781364E10
  //     },
  //     {
  //       name: '2007-09-30',
  //       value: 5.74107709999996E9
  //     },
  //     {
  //       name: '2007-06-30',
  //       value: 5.393897E9
  //     },
  //     {
  //       name: '2007-03-31',
  //       value: 5.2686008E9
  //     },
  //     {
  //       name: '2006-12-31',
  //       value: 4.59369599999996E9
  //     },
  //     {
  //       name: '2006-09-30',
  //       value: 4.261356E9
  //     },
  //     {
  //       name: '2006-06-30',
  //       value: 4.9656978E9
  //     },
  //     {
  //       name: '2006-03-31',
  //       value: 5.82928639999995E9
  //     },
  //     {
  //       name: '2005-12-31',
  //       value: 4.2103493E9
  //     },
  //     {
  //       name: '2005-09-30',
  //       value: 3.91605839999997E9
  //     },
  //     {
  //       name: '2005-06-30',
  //       value: 4.268476E9
  //     },
  //     {
  //       name: '2005-03-31',
  //       value: 4.5646244E9
  //     },
  //     {
  //       name: '2004-12-31',
  //       value: 4.8804043E9
  //     },
  //     {
  //       name: '2004-09-30',
  //       value: 4.01182969999997E9
  //     },
  //     {
  //       name: '2004-06-30',
  //       value: 5.0115065E9
  //     },
  //     {
  //       name: '2004-03-31',
  //       value: 6.96523519999994E9
  //     },
  //     {
  //       name: '2003-12-31',
  //       value: 7.53884699999994E9
  //     },
  //     {
  //       name: '2003-09-30',
  //       value: 7.1496672E9
  //     },
  //     {
  //       name: '2003-06-30',
  //       value: 8.5885332E9
  //     },
  //     {
  //       name: '2003-03-31',
  //       value: 8.56673849999992E9
  //     },
  //     {
  //       name: '2002-12-31',
  //       value: 8.442035E9
  //     },
  //     {
  //       name: '2002-09-30',
  //       value: 8.32775599999999E9
  //     },
  //     {
  //       name: '2002-06-30',
  //       value: 9.5909625E9
  //     },
  //     {
  //       name: '2002-03-31',
  //       value: 9.09128039999999E9
  //     },
  //     {
  //       name: '2001-12-31',
  //       value: 7.47342399999999E9
  //     },
  //     {
  //       name: '2001-09-30',
  //       value: 9.1364786E9
  //     },
  //     {
  //       name: '2001-06-30',
  //       value: 7.21732799999999E9
  //     },
  //     {
  //       name: '2001-03-31',
  //       value: 6.36875519999999E9
  //     },
  //     {
  //       name: '2000-12-31',
  //       value: 7.97717625063657E9
  //     },
  //     {
  //       name: '2000-09-30',
  //       value: 1.22885301470756E10
  //     },
  //     {
  //       name: '2000-06-30',
  //       value: 1.36000995311737E10
  //     },
  //     {
  //       name: '2000-03-31',
  //       value: 1.48778417870998E10
  //     },
  //     {
  //       name: '1999-12-31',
  //       value: 1.66030249943734E10
  //     },
  //     {
  //       name: '1998-12-31',
  //       value: 2.77196465E10
  //     },
  //     {
  //       name: '1997-12-31',
  //       value: 3.09620484E10
  //     },
  //     {
  //       name: '1996-12-31',
  //       value: 4.100535074E10
  //     },
  //     {
  //       name: '1995-12-31',
  //       value: 5.081771808E10
  //     }
  //   ]
  // };



  constructor(private readonly httpClient: HttpClient) {
  }

  getNominalValueGDP(): Observable<NominalValueGDP> {
    return this.httpClient.get(`${environment.apiUrl}/economic/nominalValueGDP`).pipe(map((value: any) => {
      return {
        ...value,
        endDate: new Date(value.endDate),
        startDate: new Date(value.startDate),
        series:
          value.data.map(eachData => {
            return {
              date: new Date(eachData.name),
              name: eachData.name,
              value: eachData.value
            };
          })
      };
    }));
  }
}
