import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private readonly httpClient: HttpClient) { }

  // tslint:disable-next-line:ban-types
  register(form: any): Promise<Object> {
    return this.httpClient.post(`${environment.apiUrl}/register`, form).toPromise();
  }
}
