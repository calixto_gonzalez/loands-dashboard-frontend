import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Login} from '../model/login';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  loginUrl = `${environment.apiUrl}/login`;

  constructor(private readonly httpClient: HttpClient, private router: Router) {
  }

  public static getToken(): string {
    return localStorage.getItem('token');
  }

  public static setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  public async isAuthenticated(): Promise<boolean> {
    if (AuthenticationService.getToken()) {
      return await this.httpClient.get(this.loginUrl).toPromise().then(value => {
        return true;
      }).catch(error => {
        return false;
      });
    }
    return false;
  }

  public async login(value: Login): Promise<boolean> {
    const token = btoa(`${value.email}:${value.password}`);
    return await this.httpClient.get(this.loginUrl, {headers: {Authorization: `Bearer ${token}`}}).toPromise().then(() => {
      AuthenticationService.setToken(token);
      return true;
    }).catch((error) => {
      // TODO error handling
      return false;
    });
  }

  logout() {
    AuthenticationService.setToken('');
    this.router.navigate(['/login']);
  }
}
