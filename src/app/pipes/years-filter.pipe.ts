import {Pipe, PipeTransform} from '@angular/core';
import {NominalValueGDP} from '../model/nominal-value-gdp';

@Pipe({
  name: 'yearsFilter'
})
export class YearsFilterPipe implements PipeTransform {

  transform(value: NominalValueGDP, ...args: any[]): unknown {
      const from = args[0]?.from;
      const to = args[0]?.to;
      if (from && to) {
        value.series = value.data.filter(eachSeries => new Date(eachSeries.name) >= from && new Date(eachSeries.name) <= to);
        return {...value};
      }
      return value;
  }

}
