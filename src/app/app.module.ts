import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {RegisterComponent} from './components/register/register.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbDatepickerModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {HeaderComponent} from './components/header/header.component';
import {LineChartComponent} from './components/line-chart/line-chart.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {PortalModule} from '@angular/cdk/portal';
import {SpinnerComponent} from './components/spinner/spinner.component';
import {DatePickerRangeComponent} from './components/date-picker-range/date-picker-range.component';
import {YearsFilterPipe} from './pipes/years-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
    HeaderComponent,
    SpinnerComponent,
    LineChartComponent,
    DatePickerRangeComponent,
    YearsFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxChartsModule,
    PortalModule,
    NgbDatepickerModule
  ],
  providers: [  {
    provide : HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi   : true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
