import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {DataService} from '../../services/data.service';
import {finalize, tap} from 'rxjs/operators';
import {NominalValueGDP} from '../../model/nominal-value-gdp';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  data: Observable<NominalValueGDP>;
  startData: Date;
  endDate: Date;
  loading: boolean;
  date: { year: number, month: number };
  selectedDates = {} as { from: Date, to: Date };
  refresh = new Date();

  constructor(private readonly dataService: DataService, private calendar: NgbCalendar) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.data = this.dataService.getNominalValueGDP()
      .pipe(
        tap(value => {
          this.startData = value.startDate;
          this.endDate = value.endDate;
        }))
      .pipe(
        finalize(() => {
          this.loading = false;
        })
      );

  }

  filterBySelectedFrom(from: Date): void {
    this.selectedDates.from = from;
    this.refresh = new Date();

  }

  filterBySelectedTo(to: Date): void {
    this.selectedDates.to = to;
    this.refresh = new Date();
  }
}

