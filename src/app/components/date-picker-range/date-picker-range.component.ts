import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {NgbCalendar, NgbDate, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-date-picker-range',
  templateUrl: './date-picker-range.component.html',
  styles: [`
    .form-group.hidden {
      width: 0;
      margin: 0;
      border: none;
      padding: 0;
    }
    .custom-day {
      text-align: center;
      padding: 0.185rem 0.25rem;
      display: inline-block;
      height: 2rem;
      width: 2rem;
    }
    .custom-day.focused {
      background-color: #e6e6e6;
    }
    .custom-day.range, .custom-day:hover {
      background-color: rgb(2, 117, 216);
      color: white;
    }
    .custom-day.faded {
      background-color: rgba(2, 117, 216, 0.5);
    }
  `]
})
export class DatePickerRangeComponent implements OnInit, OnChanges {

  hoveredDate: NgbDate | null = null;

  @Input() from: Date;
  @Input() to: Date;
  @Output() selectedFrom = new EventEmitter<Date>();
  @Output() selectedTo = new EventEmitter<Date>();
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  startingDate: NgbDate | null;
  endingDate: NgbDate | null;

  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      this.selectedFrom.emit(new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day));
    }
    if (this.toDate) {
      this.selectedTo.emit(new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day));
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.from.currentValue && changes.to.currentValue) {
      const startingDate = new NgbDate(changes.from.currentValue.getFullYear(), changes.from.currentValue.getMonth() + 1, changes.from.currentValue.getDate());
      const endingDate = new NgbDate(changes.to.currentValue.getFullYear(), changes.to.currentValue.getMonth() + 1, changes.to.currentValue.getDate());
      this.startingDate = JSON.parse(JSON.stringify(startingDate));
      this.fromDate = startingDate;
      this.endingDate = JSON.parse(JSON.stringify(endingDate));
      this.toDate = endingDate;
    }
  }

  markDisabled(date: NgbDate, current?: { year: number; month: number; }): boolean {
    if (this.startingDate) {
      return date.before(this.startingDate);
    }
    return true;
  }


}
