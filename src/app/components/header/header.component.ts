import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input()header: string;
  isAuthenticated: boolean;

  constructor(private readonly authenticationService: AuthenticationService) { }

  async ngOnInit(): Promise<void> {
    this.isAuthenticated = await this.authenticationService.isAuthenticated();
  }

  async logout(): Promise<void> {
    this.authenticationService.logout();
    this.isAuthenticated = await this.authenticationService.isAuthenticated();
  }
}
