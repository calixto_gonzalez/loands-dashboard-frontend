import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UsersService} from '../../services/users.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  model = {
    left: true,
    middle: false,
    right: false
  };

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private usersService: UsersService) {
    // if (this.authenticationService.currentUserValue) {
    //   this.router.navigate(['/']);
    // }
  }

  ngOnInit(): void {
    this.registerForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }

  onSubmit() {
    console.log('entro aca');
    this.submitted = true;
    // alter service
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.usersService.register(this.registerForm.value).then(() => {
      AuthenticationService.setToken(btoa(`${this.registerForm.value.email}:${this.registerForm.value.password}`));
      this.router.navigate(['']);
    }).catch((error) => {
      console.log(error);
      //TODO add slack bar
    });
  }

  get form(): FormGroup['controls'] {
    return this.registerForm.controls;
  }
}
